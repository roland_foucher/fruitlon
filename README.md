# FruitLon

## Install


### Back

Importer le fichier `initDatabase.sql` dans votre database

Créer un fichier `.env` à la racine du projet et entrer les information suivantes :

`DB_HOST=localhost`
`DB_PORT=3306`
`DB_USER=#####`
`DB_PASSWORD=#####`
`DB_DATABASE=fruitlon`
`JWT_SECRET=secret`

taper dans la console `npm i`
puis `npm start`

### Front

lien vers l'app front : `git@gitlab.com:qdurand96/fruitlon-front.git`

taper dans la console `npm i`
puis `npm start`

Enjoy 😎

