import Joi from "joi";

export class ChangePasswordDto {
  oldPassword: string;
  newPassword: string;
}

export const changePasswordDtoSchema = Joi.object({
  oldPassword: Joi.string().email().required(),
  newPassword: Joi.string().email().required(),
});