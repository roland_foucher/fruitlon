import Joi from "joi";
import { CategoryEnum } from "./CategoryEnum";
import { Recipe } from "./Recipe";
import { Season } from "./season";

export class Fruit {
  id?: number;
  name: string;
  description?: string;
  photo?: string;
  category: CategoryEnum;
  season: Season;
  vitamin: string;
  apport?: string;

  recipe?: Recipe[];


  constructor(
    name: string,
    category: CategoryEnum,
    season: Season,
    vitamin: string,
    apport?: string,
    photo?: string,
    description?: string,
    id?: number
  ) {
    this.apport = apport;
    this.category = category;
    this.description = description;
    this.id = id;
    this.name = name;
    this.photo = photo;
    this.recipe = this.recipe;
    this.season = season;
    this.vitamin = vitamin;
  }
}

export const fruitSchema = Joi.object({
  name: Joi.string().required(),
  description: Joi.string(),
  photo: Joi.string(),
  apport: Joi.string(),
  category: Joi.required(),
  season: Joi.required(),
  vitamin: Joi.string().required(),
});