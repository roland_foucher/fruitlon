import Joi from "joi";
import { Fruit } from "./Fruit";

export class Recipe {
  id?: number;
  name: string;
  description?: string;
  photo?: string;
  apport?: string;

  fruits?: Fruit[];


  constructor(
    name: string,
    description?: string,
    photo?: string,
    apport?: string,
    id?: number
  ) {
    this.apport = apport;
    this.description = description;
    this.id = id;
    this.name = name;
    this.photo = photo;
  }
}

export const recipeSchema = Joi.object({
  name: Joi.string().required(),
  description: Joi.string(),
  photo: Joi.string(),
  apport: Joi.string()
});