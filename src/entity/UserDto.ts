import { User } from "./User";

export class UserDto {
  id?: number;
  firstName?: string;
  lastName?: string;
  email?: string;

  constructor(
    user: User
  ) {

    this.firstName = user.firstName;
    this.lastName = user.lastName;
    this.id = user.id;
    this.email = user.email;
  }

}