export enum CategoryEnum {
  AGRUME,
  COQUE,
  NOYAUX,
  PEPIN,
  EXOTIQUE,
  ROUGE,
  SEC
}