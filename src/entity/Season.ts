import { SeasonEnum } from "./SeasonEnum";

export class Season {
  id?:number;
  name: SeasonEnum;
  startDate: number;
  endDate: number;

  constructor(
    name: SeasonEnum,
    startDate: number,
    endDate: number,
    id?:number
  ){
    this.endDate = endDate;
    this.id = id;
    this.name = name;
    this.startDate = startDate;
  }
}