import Joi from "joi";

export class User {
  id?: number;
  email?: string;
  password?: string;
  firstName?: string;
  lastName?: string;


  constructor(
    email: string,
    password: string,
    firstName: string,
    lastName: string,
    id: number
  ) {
    this.email = email;
    this.password = password;
    this.firstName = firstName;
    this.lastName = lastName;
    this.id = id;
  }

}

export const userSchema = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().required(),
  firstName: Joi.string(),
  lastName: Joi.string()
});