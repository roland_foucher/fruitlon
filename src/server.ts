import express from 'express';
import cors from 'cors';
import passport from 'passport';
import { userController } from './controller/user-controller';
import { configurePassport } from './utils/token';
import helmet from "helmet";
import { fruitController } from './controller/fruit-controller';
import { recipeController } from './controller/recipe-controller';
import rateLimit from 'express-rate-limit';

configurePassport();

export const server = express();

server.use(cors({
  origin: 'http://localhost:3000'
}));

server.use(helmet())
server.use(helmet.contentSecurityPolicy());
server.use(helmet.crossOriginEmbedderPolicy());
server.use(helmet.crossOriginOpenerPolicy());
server.use(helmet.crossOriginResourcePolicy());
server.use(helmet.dnsPrefetchControl());
server.use(helmet.expectCt());
server.use(helmet.frameguard());
server.use(helmet.hidePoweredBy());
server.use(helmet.hsts());
server.use(helmet.ieNoOpen());
server.use(helmet.noSniff());
server.use(helmet.originAgentCluster());
server.use(helmet.permittedCrossDomainPolicies());
server.use(helmet.referrerPolicy());
server.use(helmet.xssFilter());

server.use(passport.initialize());
server.use(express.json())

const limiter = rateLimit({
  windowMs: 15 * 60 * 1000, // 15 minutes
  max: 100, // Limit each IP to 100 requests per `window` (here, per 15 minutes)
  standardHeaders: true, // Return rate limit info in the `RateLimit-*` headers
  legacyHeaders: false, // Disable the `X-RateLimit-*` headers
})

// Apply the rate limiting middleware to all requests
server.use(limiter)

server.use('/api/user', userController);
server.use('/api/fruit', fruitController);
server.use('/api/recipe', recipeController);

server.use((req, res, next) => {
  res.status(404).send("Sorry can't find that!")
})

// custom error handler
server.use((err, req, res, next) => {
  console.error(err.stack)
  res.status(500).send('Something broke!')
})
