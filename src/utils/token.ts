import jwt from 'jsonwebtoken';
import passport from 'passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { UserRepository } from '../repository/UserRepository';
import { UserRequest } from './UserRequest';

export function generateToken(payload) {
  const token = jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: 60 * 60 * 24 * 31 });
  return token;
}

export function configurePassport() {
  passport.use(new Strategy({
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: process.env.JWT_SECRET
  }, async (payload, done) => {
    try {
      const user = await UserRepository.findByEmail(payload.email);

      if (user) {

        return done(null, user);
      }

      return done(null, false);
    } catch (error) {
      console.log(error);
      return done(error, false);
    }
  }))

}

export function protect() {

  return [
    passport.authenticate('jwt', { session: false })
  ]
}
