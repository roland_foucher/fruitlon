import { Router } from "express";
import { User, userSchema } from "../entity/User";
import { UserRepository } from "../repository/UserRepository";
import bcrypt from 'bcrypt';
import { generateToken, protect } from "../utils/token";
import { UserRequest } from "../utils/UserRequest";
import { ChangePasswordDto, changePasswordDtoSchema } from "../entity/ChangePasswordDto";
import { UserDto } from "../entity/UserDto";

export const userController: Router = Router();

userController.post('/register', async (req, res) => {
  try {
    const { error } = userSchema.validate(req.body, { abortEarly: false });

    if (error) {
      res.status(400).json({ error: error.details.map(item => item.message) });
      return;
    }

    const firstName: string = req.body.firstName;
    const lastName: string = req.body.lastName;

    if (firstName == null || lastName == null || firstName.length < 1 || lastName.length < 1) {
      res.status(400).json({ error: 'firstName or lastname not valid' });
      return;
    }

    const newUser: User = {} as User
    Object.assign(newUser, req.body);

    const exists: User = await UserRepository.findByEmail(req.body.email)
    if (exists) {
      res.status(400).json({ error: 'Email already taken' });
      return;
    }

    newUser.password = await bcrypt.hash(newUser.password, 11);

    await UserRepository.add(newUser);

    res.status(201).json({
      user: newUser.id,
      token: generateToken({
        email: newUser.email,
        id: newUser.id
      })
    });

  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});

userController.post('/login', async (req, res) => {
  try {

    const { error } = userSchema.validate(req.body, { abortEarly: false });

    if (error) {
      res.status(400).json({ error: error.details.map(item => item.message) });
      return;
    }

    const user: User = await UserRepository.findByEmail(req.body.email)

    if (user) {
      const samePassword = await bcrypt.compare(req.body.password, user.password);
      const userDto: UserDto = new UserDto(user);

      if (samePassword) {
        res.json({
          userDto,
          token: generateToken({
            email: user.email,
            id: user.id
          })
        });
        return;
      }
    }
    res.status(401).json({ error: 'Wrong email and/or password' });
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});

userController.delete('/:id', protect(), async (req, res) => {
  try {
    let toDelete: User = await UserRepository.findById(req.params.id)
    if (!toDelete) {
      res.status(404);
      return
    }

    if (req.user.id === toDelete.id) {
      await UserRepository.delete(req.params.id)
      res.status(204).end();
    } else {
      res.status(401).end();
    }
  } catch (error) {
    console.log(error);
    res.status(500).end();
  }
})

userController.get('/', protect(), (req, res) => {
  res.json(req.user);
});

userController.get('/:id', protect(), async (req: UserRequest, res) => {
  try {
    const user: User = await UserRepository.findById(req.params.id)
    const userDto: UserDto = new UserDto(user);

    if (!user) {
      res.status(404).end();
      return
    }

    if (req.user.id === user.id) {
      res.status(200).json(userDto);
    } else {
      res.status(401).end();
    }

  } catch (error) {
    console.log(error);
    res.status(400).end();
  }
})

userController.patch('/', protect(), async (req: UserRequest, res) => {
  try {
    const { error } = userSchema.validate(req.body, { abortEarly: false });

    if (error) {
      res.status(400).json({ error: error.details.map(item => item.message) });
      return;
    }

    let data: User = await UserRepository.findById(String(req.user.id))
    if (!data) {
      res.status(404).end();
      return
    }

    const samePassword: boolean = await bcrypt.compare(req.body.password, data.password);

    if (!samePassword) {
      res.status(401).end()
      return
    }
    let updated: User = { ...data, ...req.body };

    await UserRepository.update(updated)
    res.status(200).json(updated);
  } catch (error) {
    console.log(error);
    res.status(400).end();
  }
})

userController.patch('/password', protect(), async (req, res) => {
  try {


    const { error } = changePasswordDtoSchema.validate(req.body, { abortEarly: false });

    if (error) {
      res.status(400).json({ error: error.details.map(item => item.message) });
      return;
    }


    const user: User = await UserRepository.findById(String(req.user.id))
    if (!user) {
      res.status(404).end();
      return
    }

    const samePassword: boolean = await bcrypt.compare(req.body.oldPassword, user.password);

    if (samePassword) {
      const newPassword: string = await bcrypt.hash(req.body.newPassword, 11);
      await UserRepository.updatePassword(newPassword, String(user.id))
      res.status(200).end()
    }
  } catch (error) {
    console.log(error);
    res.status(400).end();
  }
})