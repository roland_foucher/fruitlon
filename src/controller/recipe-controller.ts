import { Router } from "express";
import { RecipeRepository } from "../repository/RecipeRepository";
import { protect } from "../utils/token";

export const recipeController: Router = Router();

recipeController.get('/', protect(), async (req, res) => {
  try {
    let recipes;
    if (req.query.search) {
      recipes = await RecipeRepository.searchByString(req.query.search)
    } else {
      recipes = await RecipeRepository.findAll();
    }

    res.status(200).json(recipes);
  } catch (error) {
    res.status(500).json(error);
  }

})

recipeController.get('/:id', protect(), async (req, res) => {
  try {
    const recipe = await RecipeRepository.findById(req.params.id);
    res.status(200).json(recipe);
  } catch (error) {
    res.status(500).json(error);
  }

})