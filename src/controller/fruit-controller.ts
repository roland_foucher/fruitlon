import { Router } from "express";
import { FruitRepository } from "../repository/FruitRepository";
import { protect } from "../utils/token";

export const fruitController: Router = Router();

fruitController.get('/', protect(), async (req, res) => {
  try {

    let fruits;
    if (req.query.search) {
      fruits = await FruitRepository.searchByString(req.query.search)
    } else {
      fruits = await FruitRepository.findAll();
    }

    res.status(200).json(fruits);
  } catch (error) {
    res.status(500).json(error);
  }

})

fruitController.get('/:id', protect(), async (req, res) => {
  try {
    const recipe = await FruitRepository.findById(req.params.id);
    res.status(200).json(recipe);
  } catch (error) {
    res.status(500).json(error);
  }
})