import { RowDataPacket } from "mysql2";
import { Fruit } from "../entity/Fruit";
import { Recipe } from "../entity/Recipe";
import { connection } from "./connection";
import { RecipeRepository } from "./RecipeRepository";
import { SeasonRepository } from "./SeasonRepository";

export class FruitRepository {

  private static readonly addQuery = 'INSERT INTO fruit (name,category,id_season,vitamin,apport,photo,description) VALUES (?,?,?,?,?,?,?,?)';
  private static readonly deleteQuery = 'DELETE FROM fruit WHERE id = ?';
  private static readonly updateQuery = 'UPDATE fruit SET name = ?, category = ?, id_season = ?, vitamin = ?, apport = ?, photo = ?, description = ?, WHERE id = ?';
  private static readonly findByIdQuery = 'SELECT * FROM fruit WHERE id = ?';
  private static readonly findAllQuery = 'SELECT * FROM fruit';
  private static readonly findAllByrecipeIdQuery = 'SELECT f.* FROM fruit f LEFT JOIN recipe_fruit rf  on rf.id_fruit  = f.id  where rf.id_recipe = ?';
  private static readonly saveFruitToRecipesQuery = 'INSERT IGNORE INTO recipe_fruit (id_fruit, id_recipe) VALUES (?,?)';
  private static readonly findByStringQuery = 'SELECT * FROM fruit WHERE name LIKE ? OR category LIKE ? OR vitamin LIKE ? OR apport LIKE ? OR description LIKE ?';




  static async add(fruit: Fruit): Promise<Fruit> {

    const fruitAdd = [
      fruit.name,
      fruit.category,
      fruit.season.id,
      fruit.vitamin,
      fruit.apport,
      fruit.photo,
      fruit.description,
    ]

    const [rows]: any[] = await connection.execute<RowDataPacket[]>(this.addQuery, fruitAdd);
    rows[0]['id'] = rows.insertId;
    if (fruit.recipe.length > 0) {
      fruit.recipe.forEach(async el => {
        await connection.execute<RowDataPacket[]>(this.saveFruitToRecipesQuery, [fruit.id, el.id]);
      })
    }
    return fruit;
  };

  static async delete(id: string): Promise<void> {
    await connection.execute<RowDataPacket[]>(this.deleteQuery, [id])
  };

  static async update(fruit: Fruit) {
    const fruitUpdate = [
      fruit.name,
      fruit.category,
      fruit.season.id,
      fruit.vitamin,
      fruit.apport,
      fruit.photo,
      fruit.description,
      fruit.id
    ]

    const [rows]: any[] = await connection.execute<RowDataPacket[]>(this.updateQuery, fruitUpdate);

    if (fruit.recipe.length > 0) {
      fruit.recipe.forEach(async el => {
        await connection.execute<RowDataPacket[]>(this.saveFruitToRecipesQuery, [fruit.id, el.id]);
      })
    }
    return fruit;
  };


  static async findById(id: string): Promise<Fruit> {
    const [rows]: any[] = await connection.execute<RowDataPacket[]>(this.findByIdQuery, [id]);
    if (rows.length === 0) {
      return
    }
    const fruit = await this.construct(rows[0]);
    fruit.recipe = await RecipeRepository.findAllByFruitId(fruit.id.toString());
    return fruit;
  }

  static async findAll(): Promise<Fruit[]> {
    const [rows]: any[] = await connection.execute<RowDataPacket[]>(this.findAllQuery);
    if (rows.length === 0) {
      return
    }
    const fruits: Fruit[] = [];
    for (let i = 0; i < rows.length; i++) {
      fruits.push(await this.construct(rows[i]));
    }
    return fruits;
  }

  static async findAllByRecipeId(id: string): Promise<Fruit[]> {
    const [rows]: any[] = await connection.execute<RowDataPacket[]>(this.findAllByrecipeIdQuery, [id]);
    if (rows.length === 0) {
      return
    }
    const fruits: Fruit[] = [];
    for (let i = 0; i < rows.length; i++) {
      fruits.push(await this.construct(rows[i]))
    }
    return fruits;
  }

  static async searchByString(search: string): Promise<Fruit[]> {

    search = `%${search}%`;
    const [rows]: any[] = await connection.execute<RowDataPacket[]>(this.findByStringQuery, [search, search, search, search, search]);
    if (rows.length === 0) {
      return
    }
    const fruits: Fruit[] = [];
    for (let i = 0; i < rows.length; i++) {
      fruits.push(await this.construct(rows[i]))
    }
    return fruits;
  }

  private static async construct(row: any[]): Promise<Fruit> {


    const arr = new Uint8Array(row['photo']);
    const stringArr = arr.reduce((data, byte) => {
      return data + String.fromCharCode(byte)
    }, '');

    return new Fruit(
      row['name'],
      row['category'],
      await SeasonRepository.findById(row['id_season']),
      row['vitamin'],
      row['apport'],
      stringArr,
      row['description'],
      row['id']
    )
  }
}