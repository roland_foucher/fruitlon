import { RowDataPacket } from "mysql2";
import { Recipe } from "../entity/Recipe";
import { connection } from "./connection";
import { FruitRepository } from "./FruitRepository";

export class RecipeRepository {

  private static readonly addQuery = 'INSERT INTO recipe (name,description,photo,apport) VALUES (?,?,?,?)';
  private static readonly deleteQuery = 'DELETE FROM recipe WHERE id = ?';
  private static readonly updateQuery = 'UPDATE recipe SET name = ?, description = ?, photo = ?, apport = ? WHERE id = ?';
  private static readonly findByIdQuery = 'SELECT * FROM recipe WHERE id = ?';
  private static readonly findAllQuery = 'SELECT * FROM recipe';
  private static readonly findAllByFruitIdQuery = 'SELECT r.* FROM recipe r LEFT JOIN recipe_fruit rf  on rf.id_recipe  = r.id  where rf.id_fruit = ?';
  private static readonly findByStringQuery = 'SELECT * FROM recipe WHERE name LIKE ? OR description LIKE ? OR apport LIKE ?';



  static async add(recipe: Recipe): Promise<Recipe> {

    const recipeAdd = [
      recipe.name,
      recipe.description,
      recipe.photo,
      recipe.apport,
    ]

    const [rows]: any[] = await connection.execute<RowDataPacket[]>(this.addQuery, recipeAdd);
    rows[0]['id'] = rows.insertId;
    return this.construct(rows[0]);
  };

  static async delete(id: string): Promise<void> {
    await connection.execute<RowDataPacket[]>(this.deleteQuery, [id])
  };

  static async update(recipe: Recipe) {
    const recipeUpdate = [
      recipe.name,
      recipe.description,
      recipe.photo,
      recipe.apport,
      recipe.id
    ]
    const [rows]: any[] = await connection.execute<RowDataPacket[]>(this.updateQuery, recipeUpdate);
    return this.construct(rows[0]);
  };


  static async findById(id: string): Promise<Recipe> {
    const [rows]: any[] = await connection.execute<RowDataPacket[]>(this.findByIdQuery, [id]);
    if (rows.length === 0) {
      return
    }
    const recipe: Recipe = this.construct(rows[0]);
    recipe.fruits = await FruitRepository.findAllByRecipeId(recipe.id.toString());
    return recipe;
  }

  static async findAll(): Promise<Recipe[]> {
    const [rows]: any[] = await connection.execute<RowDataPacket[]>(this.findAllQuery);
    if (rows.length === 0) {
      return
    }
    const recipes: Recipe[] = [];
    for (let i = 0; i < rows.length; i++) {

      recipes.push(this.construct(rows[i]))
    }
    return recipes;
  }

  static async findAllByFruitId(fruitId: string): Promise<Recipe[]> {
    const [rows]: any[] = await connection.execute<RowDataPacket[]>(this.findAllByFruitIdQuery, [fruitId]);
    if (rows.length === 0) {
      return
    }
    const recipes: Recipe[] = [];
    for (let i = 0; i < rows.length; i++) {
      recipes.push(this.construct(rows[i]))
    }
    return recipes;
  }

  static async searchByString(search: string): Promise<Recipe[]> {

    search = `%${search}%`;
    const [rows]: any[] = await connection.execute<RowDataPacket[]>(this.findByStringQuery, [search, search, search]);
    if (rows.length === 0) {
      return
    }
    const recipes: Recipe[] = [];
    for (let i = 0; i < rows.length; i++) {
      recipes.push(await this.construct(rows[i]))
    }
    return recipes;
  }

  private static construct(row: any[]): Recipe {


    const arr = new Uint8Array(row['photo']);
    const stringArr = arr.reduce((data, byte) => {
      return data + String.fromCharCode(byte)
    }, '');

    return new Recipe(
      row['name'],
      row['description'],
      stringArr,
      row['apport'],
      row['id']
    )
  }


}