import { RowDataPacket } from "mysql2";
import { Season } from "../entity/season";
import { connection } from "./connection";

export class SeasonRepository {

    private static readonly findByIdQuery = 'SELECT * FROM season WHERE id = ?';
    private static readonly findAllQuery = 'SELECT * FROM season';
    
    static async findById(id:string):Promise<Season> {
        const [rows]:any[] = await connection.execute<RowDataPacket[]>(this.findByIdQuery, [id]);
        if(rows.length===0){
            return
        }
        return this.construct(rows[0]);
    }

    static async findAll(id:string):Promise<Season[]> {
      const [rows]:any[] = await connection.execute<RowDataPacket[]>(this.findAllQuery, [id]);
      if(rows.length===0){
          return
      }
      const season : Season[] = [];
      for (let i = 0; i < rows.length; i++) {
        season.push(this.construct(rows[i]))
      }
      return season;
    }

    private static construct(row: any[]) : Season {
      return new Season(
        row['name'], 
        row['startDate'], 
        row['endDate'], 
        row['id']
      )
    }
}