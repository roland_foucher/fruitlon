import { RowDataPacket } from "mysql2";
import { User } from "../entity/User";
import { connection } from "./connection";

export class UserRepository {

    static async add(user:User) {
        const [rows]:any[] = await connection.execute<RowDataPacket[]>('INSERT INTO user (firstName,lastName,email,password) VALUES (?,?,?,?)', [user.firstName, user.lastName, user.email, user.password]);
        user.id= rows.insertId;
    };

    static async delete(id:string){
        await connection.execute<RowDataPacket[]>(`DELETE FROM user WHERE id = ?`,[id])
    };

    static async update(user:User){
        await connection.execute<RowDataPacket[]>('UPDATE user SET email = ?, firstName = ?, lastName = ?, WHERE id = ?', [user.email, user.firstName, user.lastName, user.id]);
    };

    static async updatePassword(password:string,userId:string){
        await connection.execute<RowDataPacket[]>('UPDATE user SET password = ? WHERE id = ?', [password, userId]);
    };

    static async findByEmail(email:string):Promise<User> {
        const [rows]:any[] = await connection.execute<RowDataPacket[]>('SELECT * FROM user WHERE email = ?', [email]);
        if(rows.length === 1) {
            return new User(rows[0]['email'], rows[0]['password'], rows[0]['firstName'], rows[0]['lastName'], rows[0]['id']);
        };
        return null;
    };

    static async findById(id:string):Promise<User> {
        const [rows]:any[] = await connection.execute<RowDataPacket[]>(`SELECT * FROM user WHERE id = ?`, [id]);
        if(rows.length===0){
            return
        }
        return new User(rows[0]['email'], rows[0]['password'], rows[0]['firstName'], rows[0]['lastName'], rows[0]['id']);
    }
}